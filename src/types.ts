type MutationTypes = "bold" | "italics" | "strike" | "underline";

type LeafPerms = {
  uid: string;
  text: string;
};
type MutationBools = Partial<Record<MutationTypes, boolean>>;

type LeafNode = LeafPerms & MutationBools;

type EditorBlock = {
  // for multiple paragraphs
  uid: string;
  type: "paragraph";
  contents: Array<LeafNode>;
  // children
};

type EditorState = {
  initialValue: Array<EditorBlock>;
  // other editor state stuff
};

type Details = {
  parent: HTMLElement | undefined;
  selection: Selection;
};

export type { Details, LeafNode, EditorBlock, EditorState, MutationTypes };
