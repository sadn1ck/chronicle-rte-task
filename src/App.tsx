import { Editor } from "./components/Editor";
import { EditorBlock } from "./types";

function App() {
  const initialValue: EditorBlock[] = [
    {
      uid: "f55fcf6",
      type: "paragraph",
      contents: [
        {
          uid: "e6c333f",
          text: "The quick brown fox jumps over the lazy dog.",
        },
      ],
    },
  ];
  return (
    <>
      <Editor state={{ initialValue }} />
    </>
  );
}

export default App;
