import { nanoidCustom } from "../nanoid";
import { MouseEvent, useCallback, useEffect, useState } from "react";
import type {
  EditorState,
  EditorBlock,
  MutationTypes,
  LeafNode,
  Details,
} from "../types";
import { Popover } from "./Popover";
import { Leaf, Element } from "./Blocks";
import "./Editor.css";

export const Editor = ({ state }: { state: EditorState }) => {
  // used https://github.com/juliankrispel/use-text-selection
  const [target, setTarget] = useState<HTMLElement>();

  const ref = useCallback((el) => {
    if (el !== null) {
      setTarget(el);
    } else {
      setTarget(undefined);
    }
  }, []);
  // destructure
  const { initialValue } = state;
  // dont work on props, use derived state
  const [derivedContent, setContent] = useState(initialValue);
  // send active mutations for toggling
  const [activeMuts, setActive] = useState<MutationTypes[]>([]);
  // simple state for selection and parent node
  const [details, setDetails] = useState<Details>();

  const selectionChangeHandler = () => {
    const selection = window.getSelection()!;
    const parent = selection?.anchorNode?.parentElement!;
    /**
     * one drawback of this method is that I can only check
     * the style of the node where the selection starts
     * if selection spans elements, then it breaks down
     *
     * I am unsure of how to solve this particular problem
     */
    setDetails({ parent, selection });
    const nodeId = parent?.dataset?.id!;
    // check selection style
    derivedContent.forEach((content) => {
      content.contents.forEach((leaf, idx) => {
        if (leaf.uid === nodeId) {
          const muts: MutationTypes[] = [];
          // set the active mutations on currently selected text
          if (leaf.bold) {
            muts.push("bold");
          }
          if (leaf.italics) {
            muts.push("italics");
          }
          if (leaf.strike) {
            muts.push("strike");
          }
          if (leaf.underline) {
            muts.push("underline");
          }
          setActive(muts);
        }
      });
    });
  };

  useEffect(() => {
    document.addEventListener("selectionchange", selectionChangeHandler);
    console.log("Listening to selectionchange");
    // cleanup
    return () =>
      document.removeEventListener("selectionchange", selectionChangeHandler);
  }, []);

  const clickHandler = (
    e: MouseEvent<HTMLButtonElement>,
    action: MutationTypes
  ) => {
    const nodeId = details?.parent?.dataset?.id!;
    const selectedText = details?.selection?.toString()!;

    derivedContent.forEach((content) => {
      content.contents.forEach((leaf, idx) => {
        const [before, after] = leaf.text.split(selectedText);
        if (leaf.uid === nodeId && !activeMuts.includes(action)) {
          console.log(`before: "${before}" after: "${after}"`, leaf, nodeId);
          if (selectedText === leaf.text) {
            // if selection text matches node text, just flip action
            leaf[action] = true;
          } else {
            // unstyled
            if (before?.length > 0 && after?.length > 0) {
              console.log("case 1");
              // case 1
              // before, after both len > 0
              leaf.text = after;
              const newStyledNode: LeafNode = {
                uid: nanoidCustom(),
                text: selectedText,
              };
              newStyledNode[action] = !newStyledNode[action];
              const beforeNode: LeafNode = {
                uid: nanoidCustom(),
                text: before,
              };
              content.contents.splice(idx, 0, newStyledNode);
              content.contents.splice(idx, 0, beforeNode);
            } else {
              // splitting first word
              if (before === "") {
                leaf.text = after;
                const newStyledNode: LeafNode = {
                  uid: nanoidCustom(),
                  text: selectedText,
                };
                newStyledNode[action] = !newStyledNode[action];
                content.contents.splice(idx, 0, newStyledNode);
              } else if (after === "") {
                // splitting last word
                leaf.text = selectedText;
                const beforeNode: LeafNode = {
                  uid: nanoidCustom(),
                  text: before,
                };
                leaf[action] = !leaf[action];
                content.contents.splice(idx, 0, beforeNode);
              }
            }
          }
          setContent(derivedContent);
        }
        // already styled text
        if (leaf.uid === nodeId && activeMuts.includes(action)) {
          // toggle
          leaf[action] = !leaf[action];
          setContent(derivedContent);
        }
      });
    });
    window.getSelection()?.removeAllRanges();
  };

  const getChildrenFrom = (block: EditorBlock) => {
    return (
      <>
        {block.contents.map((content) => {
          return (
            <Leaf
              key={content.uid}
              leaf={content}
              children={`${content.text}`}
            />
          );
        })}
      </>
    );
  };

  return (
    <div className="editor">
      <div ref={ref}>
        {derivedContent.map((block) => {
          const children = getChildrenFrom(block);
          return (
            <Element key={block.uid} element={block} children={children} />
          );
        })}
      </div>
      <Popover
        activeMutations={activeMuts}
        target={target}
        onClick={clickHandler}
      />
    </div>
  );
};
