import { EditorBlock, LeafNode } from "../types";

const Element = ({
  children,
  element,
}: {
  children: React.ReactNode;
  element: EditorBlock;
}) => {
  switch (element.type) {
    case "paragraph":
      return <p>{children}</p>;
    default:
      return <div>{children}</div>;
  }
};

const Leaf = ({
  children,
  leaf,
}: {
  children: React.ReactNode;
  leaf: LeafNode;
}) => {
  if (leaf.bold) {
    children = <strong data-id={leaf.uid}>{children}</strong>;
  }

  if (leaf.italics) {
    children = <em data-id={leaf.uid}>{children}</em>;
  }

  if (leaf.underline) {
    children = <u data-id={leaf.uid}>{children}</u>;
  }

  if (leaf.strike) {
    children = <s data-id={leaf.uid}>{children}</s>;
  }

  return <span data-id={leaf.uid}>{children}</span>;
};

export { Leaf, Element };
