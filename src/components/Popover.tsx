// used https://github.com/juliankrispel/use-text-selection

import { useTextSelection } from "use-text-selection";
import { createPortal } from "react-dom";
import { MutationTypes } from "../types";
import { MouseEvent } from "react";

import { FaBold, FaItalic, FaUnderline, FaStrikethrough } from "react-icons/fa";

const Portal = ({ children }: { children: React.ReactNode }) => {
  return createPortal(children, document.body);
};

type PopoverClickHandlerProps = {
  onClick: (e: MouseEvent<HTMLButtonElement>, action: MutationTypes) => any;
  activeMutations: MutationTypes[];
  target?: HTMLElement;
};

export const Popover = (props: PopoverClickHandlerProps) => {
  const { target, onClick, activeMutations } = props;
  const { isCollapsed, clientRect } = useTextSelection(target);
  const actions: { name: MutationTypes; Icon: JSX.Element }[] = [
    {
      name: "bold",
      Icon: <FaBold size={18} />,
    },
    {
      name: "italics",
      Icon: <FaItalic size={18} />,
    },
    {
      name: "strike",
      Icon: <FaStrikethrough size={18} />,
    },
    {
      name: "underline",
      Icon: <FaUnderline size={18} />,
    },
  ];

  if (clientRect == null || isCollapsed) return null;

  /**
   * Explanation for shortcuts
   *
   * For Ctrl + B for bold
   * For Ctrl + I for italics
   * For Ctrl + U for underline
   * For Ctrl + S for strikethrough
   *
   * Steps to take:
   * - Write a custom hook/use a library for getting currently pressed keys
   * - Check if the popover is visible
   * - If popover is visible, and a combination of the above keys is pressed
   * - call the onClick prop passed to the popover with the appropriate action
   * - functionality should be the same as a click event as all the shortcut does is call a function
   */

  return (
    <Portal>
      <span
        style={{
          position: "absolute",
          left: `${clientRect.left + clientRect.width / 2}px`,
          top: `${clientRect.top - 50}px`,
          marginLeft: "50px",
          borderRadius: "24px",
        }}
        className="bg-gray-300 pt-2 ring-2 ring-gray-400 rounded-xl px-3"
      >
        {actions.map((action) => {
          return (
            <button
              onClick={(e) => onClick(e, action.name)}
              className={`p-2 opacity-30 hover:opacity-100 rounded-full ${
                activeMutations.includes(action.name)
                  ? "opacity-100"
                  : "opacity-300"
              }`}
              key={action.name}
            >
              {action.Icon}
            </button>
          );
        })}
      </span>
    </Portal>
  );
};
