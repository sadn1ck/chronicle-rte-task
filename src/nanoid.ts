import { customAlphabet } from "nanoid";

export const nanoidCustom = customAlphabet("1234567890abcdef", 7);
