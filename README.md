# Rich Text Editor - Task

## [Hosted Version Link](https://chronicle-rte-task-sadn1ck.vercel.app)

> Didn't commit much and did it in one go, so there's zero commit history

## Setup

```sh

# clone, cd into folder

$ npm install

$ npm run dev

```

## Objectives completed

- [x] Enable styles via popup
- [x] Toggle styles via popup
- [ ] Enable/toggle styles via keyboard shortcuts (thoughts commented in [Popover.tsx](./src/components/Popover.tsx#L45))

## Data Model Chosen

I chose a tree like structure to represent the text and its styles.

The document consists of multiple `EditorBlock`, which in turn consists of the `contents` and a unique `uid`.

Each content is a `LeafNode` with uid, text and style options.

```ts
type MutationTypes = "bold" | "italics" | "strike" | "underline";

type LeafPerms = {
  uid: string;
  text: string;
};
// optional keys for each node, based on the style
type MutationBools = Partial<Record<MutationTypes, boolean>>;

// each node has text, uid, and either of the 4 styles
type LeafNode = LeafPerms & MutationBools;

// each block consists of multiple leaves
type EditorBlock = {
  // for multiple paragraphs
  uid: string;
  type: "paragraph";
  contents: Array<LeafNode>;
  // children
};
```

## [Video (not sure if it will work)](https://gitlab.com/sadn1ck/chronicle-rte-task/-/blob/main/task.mp4)

<figure class="video_container">
  <iframe src="https://gitlab.com/sadn1ck/chronicle-rte-task/-/raw/main/task.mp4" frameborder="0" allowfullscreen="true"> </iframe>
</figure>

### Pros of the model

- Easy to represent
- JSON like structure, hence easy to grok at a glance
- Similar to HTML (both tree representations)

### Cons of the model

- Selecting text across multiple nodes is pretty involved, includes breaking down nodes across multiple parents, was not able to solve

### Thoughts

Overall was a fun task, spent a little over 3 hours which included writing the README and comments as well.
